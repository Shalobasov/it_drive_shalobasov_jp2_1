import java.util.*;

public class MyLinkedList<T> implements List<T> {
    Item<T> first = null;
    Item<T> last = null;
    private int size;

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public boolean contains(Object o) {
        Item<T> f = first;
        while (f.nextItem != null) {
            if (f.element.equals(o)) {
                return true;
            }
            f = f.nextItem;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyIterator(0);
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Item<T> x = first; x != null; x = x.nextItem) {
            result[i++] = x.element;
        }
        return result;
    }

    @Override
    public <E> E[] toArray(E[] a) {
        if (a.length < size) {
            a = (E[]) java.lang.reflect.Array.newInstance(a.getClass().getComponentType(), size);
        }
        int i = 0;
        Object[] result = a;
        for (Item<T> x = first; x != null; x = x.nextItem) {
            result[i++] = x.element;
        }

        if (a.length > size) {
            a[size] = null;
        }

        return a;
    }

    @Override
    public boolean add(T t) {
        if (this.size() == 0) {
            this.first = new Item<>(t, null, null);
            this.last = first;
        } else if (this.size() == 1) {
            this.last = new Item<>(t, this.first, null);
            this.first.nextItem = this.last;
        } else {
            final Item<T> newLast = new Item<>(t, this.last, null);
            this.last.nextItem = newLast;
            this.last = newLast;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (size == 0) return false;

        Item<T> current = first;
        while (current.nextItem != null && !current.element.equals(o)) {
            current = current.nextItem;
        }

        if (current.element.equals(o)) {
            if (this.size() == 1) {
                first = null;
                last = null;
            } else {
                if (current == first) {
                    first = current.nextItem;
                    first.prevItem = null;
                }
                if (current == last) {
                    last = current.prevItem;
                    last.nextItem = null;
                }
                if (current.nextItem != null && current.prevItem != null) {
                    current.prevItem.nextItem = current.nextItem;
                    current.nextItem.prevItem = current.prevItem;
                }
            }
            size--;
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (final Object item : c) {
            if (!this.contains(item)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (final T item : c) {
            add(item);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (final Object item : c) {
            remove(item);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (final T item : this) {
            if (!c.contains(item)) this.remove(item);
        }
        return true;
    }

    @Override
    public void clear() {
        this.first = null;
        this.last = null;
        size = 0;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }

        int i = 0;
        Item<T> current = first;
        while (i != index) {
            current = current.nextItem;
            i++;
        }
        return current.element;
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }

        Item<T> item = getItemByIndex(index);
        T tempElement = item.element;
        item.element = element;
        return tempElement;
    }

    private Item<T> getItemByIndex(final int index) {
        int i = 0;
        Item<T> current = first;
        while (i != index) {
            current = current.nextItem;
            i++;
        }
        return current;
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int index) {
        int i = 0;
        if (size <= index || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        Item<T> current = this.first;

        while (i != index) {
            current = current.nextItem;
            i++;
        }

        if (current != null) {
            if (this.size() == 1) {
                this.first = null;
                this.last = null;
            } else {
                if (current == first) {
                    first = current.nextItem;
                    first.prevItem = null;
                }
                if (current == last) {
                    last = current.prevItem;
                    last.nextItem = null;
                }
                if (current.nextItem != null && current.prevItem != null) {
                    current.prevItem.nextItem = current.nextItem;
                    current.nextItem.prevItem = current.prevItem;
                }
            }
            size--;
            return current.element;
        }
        return null;
    }

    @Override
    public int indexOf(Object o) {
        int i = 0;
        if (o == null) {
            for (Item<T> current = first; current != null; current = current.nextItem) {
                if (current.element == null)
                    return i;
                i++;
            }
        } else {
            for (Item<T> current = first; current != null; current = current.nextItem) {
                if (o.equals(current.element))
                    return i;
                i++;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

    private static class Item<T> {
        private T element;
        private Item<T> nextItem;
        private Item<T> prevItem;

        Item(final T element, final Item<T> prevItem, final Item<T> nextItem) {
            this.element = element;
            this.nextItem = nextItem;
            this.prevItem = prevItem;
        }

        public Item<T> getNextItem() {
            return nextItem;
        }

        public Item<T> getPrevItem() {
            return prevItem;
        }

        public T getElement() {
            return element;
        }
    }

    class MyIterator implements Iterator<T> {
        private Item<T> currentItemInIterator;
        private Item<T> lastReturnedItemFromIterator;
        private int index;

        public MyIterator(int index) {
            this.currentItemInIterator = (index == size) ? null : getItemByIndex(index);
            this.index = index;
        }

        @Override
        public boolean hasNext() {
            return this.index < size;
        }

        @Override
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            lastReturnedItemFromIterator = currentItemInIterator;
            currentItemInIterator = currentItemInIterator.getNextItem();
            index++;
            return lastReturnedItemFromIterator.element;
        }
    }

    class MyListIterator implements ListIterator<T> {
        private Item<T> currentItemInIterator;
        private Item<T> lastReturnedItemFromIterator;
        private int index;

        public MyListIterator(final int index) {
            this.currentItemInIterator = (index == size) ? null : getItemByIndex(index);
            this.index = index;
        }

        @Override
        public boolean hasNext() {
            return this.index < size;
        }

        @Override
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            lastReturnedItemFromIterator = currentItemInIterator;
            currentItemInIterator = currentItemInIterator.getNextItem();
            index++;
            return lastReturnedItemFromIterator.element;
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public T previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }

            if (currentItemInIterator == null) {
                lastReturnedItemFromIterator = currentItemInIterator = last;
            } else {
                lastReturnedItemFromIterator = currentItemInIterator = currentItemInIterator.getPrevItem();
            }

            return lastReturnedItemFromIterator.element;
        }

        @Override
        public int nextIndex() {
            return index;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            if (lastReturnedItemFromIterator == null) {
                throw new IllegalStateException();
            }
            MyLinkedList.this.remove(lastReturnedItemFromIterator);
            lastReturnedItemFromIterator = null;
            index--;
        }

        @Override
        public void set(T t) {
            if (lastReturnedItemFromIterator == null) {
                throw new IllegalStateException();
            }
            lastReturnedItemFromIterator.element = t;
        }

        @Override
        public void add(T t) {
            throw new UnsupportedOperationException();
        }
    }
}