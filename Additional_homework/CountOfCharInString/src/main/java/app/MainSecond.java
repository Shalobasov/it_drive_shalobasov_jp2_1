public class MainSecond {
    public static void main(String[] args) {
        String result = "aabbbcasdasdasdasdsadac";
        printMaxNumberOfCharInString(result);
    }

    public static void printMaxNumberOfCharInString(String currentString) {
        int currentChar = 0;
        int maxNumberOfElements = 0;
        int currentNumberOfElements = 0;
        char[] charMass = currentString.toCharArray();
        int current = 0;
        int i = 0;
        while (i < charMass.length) {
            if (charMass[i] == charMass[current]) {
                currentNumberOfElements++;
            }
            if (current == charMass.length - 1) {
                if (currentNumberOfElements > maxNumberOfElements) {
                    currentChar = i;
                    maxNumberOfElements = currentNumberOfElements;
                }
                currentNumberOfElements = 0;
                current = 0;
                i++;
            }
            current++;
        }
        System.out.println(charMass[currentChar] + " " + currentNumberOfElements);
    }
}
