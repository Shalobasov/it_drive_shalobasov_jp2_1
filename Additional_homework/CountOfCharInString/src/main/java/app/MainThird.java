public class MainThird {
    public static void main(String[] args) {
        String result = "abccbaa";
        printMaxNumberOfCharInString(result);
    }

    public static void printMaxNumberOfCharInString(String currentString) {
        int maxCharIndex = 0;
        int maxNumberOfElements = 0;
        char[] allChars = new char[65535];
        for (int i = 0; i < currentString.toCharArray().length; i++) {
            allChars[currentString.charAt(i) - 1]++;
        }

        for (int i = 64; i < 122; i++) {
            if (maxNumberOfElements < allChars[i]) {
                maxNumberOfElements = allChars[i];
                maxCharIndex = i;
            }
        }
        System.out.println((char) (maxCharIndex + 1) + " " + maxNumberOfElements);
    }
}
