public class Main {
    public static void main(String[] args) {
        String result = "aabbbcccddddssabba";
        printMaxNumberOfCharInString(result);
    }

    public static void printMaxNumberOfCharInString(String currentString) {
        int currentChar = 0;
        int maxNumberOfElements = 0;
        int currentNumberOfElements = 0;
        for (int i = 0; i < currentString.toCharArray().length; i++) {
            for (int j = i; j < currentString.toCharArray().length; j++) {
                if (currentString.charAt(i) == currentString.charAt(j)) {
                    currentNumberOfElements++;
                }
            }
            if (maxNumberOfElements < currentNumberOfElements) {
                currentChar = i;
                maxNumberOfElements = currentNumberOfElements;
            }
            currentNumberOfElements = 0;
        }
        System.out.println(currentString.charAt(currentChar) + " " + maxNumberOfElements);
    }
}
