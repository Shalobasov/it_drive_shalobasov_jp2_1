package utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

public class FileDownloader {
    public void downloadFile(String url, String srcDir, String name, String imageExtension) {
        try {
            URL siteUrl = new URL(url);
            BufferedImage image = ImageIO.read(siteUrl);
            ImageIO.write(image, imageExtension, new File(srcDir + File.separator + name + "." + imageExtension));
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}