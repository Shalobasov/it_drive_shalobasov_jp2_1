package utils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Argument {
    @Parameter(names = {"--url"})
    String url;

    @Parameter(names = {"--src"})
    String srcDir;

    @Parameter(names = {"--name"})
    String imageName;

    @Parameter(names = {"--ext"})
    String imageExtension;

    public String getUrl() {
        return url;
    }

    public String getSrcDir() {
        return srcDir;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImageExtension() {
        return imageExtension;
    }
}