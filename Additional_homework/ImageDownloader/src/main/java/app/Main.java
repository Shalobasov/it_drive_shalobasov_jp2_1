package app;

import com.beust.jcommander.JCommander;
import utils.Argument;
import utils.FileDownloader;

public class Main {
    public static void main(String[] args) {
        FileDownloader fileDownloader = new FileDownloader();
        Argument argument = new Argument();

        JCommander
                .newBuilder()
                .addObject(argument)
                .build()
                .parse(args);

        fileDownloader.downloadFile(
                argument.getUrl(),
                argument.getSrcDir(),
                argument.getImageName(),
                argument.getImageExtension());
    }
}