CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name char(20),
    surname char(20),
    email char(30),
    password char(10));

INSERT INTO users(name, surname, email, password)
    VALUES ('first', 'firstov', 'first@mail.ru', 'first');
INSERT INTO users(name, surname, email, password) 
    VALUES ('second', 'secondov', 'second@mail.ru', 'second');

CREATE TABLE chat (id serial primary key,
                   name char(20),
                   create_date timestamp);

INSERT INTO chat (name, create_date) VALUES ('first', '2020-01-10');
INSERT INTO chat (name, create_date) VALUES ('second', '2020-02-10');

CREATE TABLE users_chats (user_id integer,
                          chat_id integer,
                          foreign key (user_id) references users(id),
                          foreign key (chat_id) references chat(id));

CREATE TABLE message (id serial primary key,
                      text char(200),
                      author char(10),
                      user_id integer,
                      chat_id integer,
                      foreign key (user_id) references users(id),
                      foreign key (chat_id) references chat(id));

INSERT INTO message (text, author, user_id, chat_id) VALUES ('hello first chat', 'michale', 1, 1);