package utils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.FileConverter;

import java.io.File;
import java.util.List;

@Parameters(separators = "=")
public class Argument {
    @Parameter(names = "--files", converter = FileConverter.class, splitter = SemiColonSplitter.class)
    List<File> files;

    public List<File> getFiles() {
        return files;
    }
}