package utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileInformationApi {
    public Map<String, List<FileInformation>> getFilesInformation(File folder) {
        Map<String, List<FileInformation>> map = new HashMap<>();
        List<FileInformation> fileInformationList = new ArrayList<>();
        File[] files = folder.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                continue;
            }
            fileInformationList.add(new FileInformation(file.getName(), file.length()));
        }
        map.put(folder.getName(), fileInformationList);
        return map;
    }

}