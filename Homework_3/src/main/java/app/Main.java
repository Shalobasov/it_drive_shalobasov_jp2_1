package app;

import com.beust.jcommander.JCommander;
import utils.Argument;
import utils.FileInformationApi;

import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Argument argument = new Argument();
        final FileInformationApi api = new FileInformationApi();

        JCommander.newBuilder().addObject(argument).build().parse(args);

        List<File> files = argument.getFiles();
        for (final File file : files) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    api.getFilesInformation(file);
                }
            }).start();
        }
    }
}