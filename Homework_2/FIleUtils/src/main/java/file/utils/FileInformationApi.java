package utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileInformationApi {
    public List<FileInformation> getFilesInformation(File folder) {
        List<FileInformation> fileInformationList = new ArrayList<>();
        File[] files = folder.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                continue;
            }
            fileInformationList.add(new FileInformation(file.getName(), file.length()));
        }
        return fileInformationList;
    }
}