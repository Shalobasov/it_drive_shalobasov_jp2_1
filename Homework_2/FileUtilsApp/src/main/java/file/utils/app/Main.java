package app;

import com.beust.jcommander.JCommander;
import utils.FileInformation;
import utils.FileInformationApi;

import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        FileInformationApi fileInformationApi = new FileInformationApi();
        Argument argument = new Argument();

        JCommander.newBuilder().addObject(argument).build().parse(args);

        List<FileInformation> filesInformation = fileInformationApi.getFilesInformation(new File(argument.getDirectoryPath()));
        for (FileInformation fileInformation : filesInformation) {
            System.out.println(fileInformation.getFileName() + " " + fileInformation.getFileSize());
        }
    }
}