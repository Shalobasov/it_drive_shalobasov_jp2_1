package app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Argument {
    @Parameter(names = {"--dirname"})
    String directoryPath;

    public String getDirectoryPath() {
        return directoryPath;
    }
}