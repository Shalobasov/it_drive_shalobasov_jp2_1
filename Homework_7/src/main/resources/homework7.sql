CREATE TABLE lesson (
    id SERIAL PRIMARY KEY,
    name char(20),
    course_id integer,
    foreign key (course_id) references course(id)
);

CREATE TABLE course(
   id SERIAL PRIMARY KEY,
   name char(20)
);

INSERT INTO course (name) VALUES ('First Course');
INSERT INTO course (name) VALUES ('SECOND Course');

INSERT INTO lesson (name, course_id) VALUES ('First lesson first course', 1);
INSERT INTO lesson (name, course_id) VALUES ('Second lesson first course', 1);
INSERT INTO lesson (name, course_id) VALUES ('Second lesson SECOND course', 2);
INSERT INTO lesson (name, course_id) VALUES ('Second lesson SECOND course', 2);