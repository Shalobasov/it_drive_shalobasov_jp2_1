package homework_seven;

import homework_seven.models.Course;
import homework_seven.models.Lesson;
import homework_seven.repositories.CourseRepository;
import homework_seven.repositories.LessonRepository;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        LessonRepository lessonRepository = new LessonRepository();
        List<Lesson> lessons = lessonRepository.findAll();
        for (Lesson l : lessons){
            System.out.println(l.getId() + l.getName() + l.getCourse().getId() + l.getCourse().getName());
        }

        CourseRepository courseRepository = new CourseRepository();
        List<Course> courses = courseRepository.findAll();

        for (Course course: courses){
            System.out.println("Course name = " + course.getName());
            for(Lesson lesson: course.getLessons()){
                System.out.println("Lesson name" + lesson.getName());
            }
        }
    }
}