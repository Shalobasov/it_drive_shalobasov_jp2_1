package homework_seven.repositories;

import homework_seven.helper.DBHelper;
import homework_seven.models.Course;
import homework_seven.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LessonRepository implements CrudRepository<Lesson> {
    private Connection connection = DBHelper.getPostgresConnection();
    private static final String SELECT_BY_ID_FROM_COURSE = "SELECT * FROM course WHERE id = ";
    private static final String SELECT_BY_ID_FROM_LESSON = "SELECT * FROM lesson WHERE id = ";
    private static final String SELECT_ALL_FROM_LESSON = "SELECT * FROM lesson";

    private RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        @Override
        public Lesson mapRow(ResultSet row) throws SQLException {
            Lesson lesson = new Lesson(row.getInt("id"), row.getString("name"));
            Statement statement = connection.createStatement();
            ResultSet resultSetCourse = statement.executeQuery(SELECT_BY_ID_FROM_COURSE + row.getInt("course_id"));
            resultSetCourse.next();
            lesson.setCourse(courseRowMapper.mapRow(resultSetCourse));

            return lesson;
        }
    };

    private RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        @Override
        public Course mapRow(ResultSet row) throws SQLException {
            return new Course(row.getInt("id"), row.getString("name"));
        }
    };

    public Lesson find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSetLesson = statement.executeQuery(SELECT_BY_ID_FROM_LESSON + id);
            resultSetLesson.next();
            return lessonRowMapper.mapRow(resultSetLesson);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Lesson> findAll() {
        try {
            List<Lesson> lessons = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet resultSetLessons = statement.executeQuery(SELECT_ALL_FROM_LESSON);
            while (resultSetLessons.next()) {
                Lesson lesson = lessonRowMapper.mapRow(resultSetLessons);
                lessons.add(lesson);
            }
            return lessons;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}