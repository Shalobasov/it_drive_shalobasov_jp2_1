package homework_seven.repositories;

import java.util.List;

public interface CrudRepository<T> {
    T find(Integer id);
    List<T> findAll();
}
