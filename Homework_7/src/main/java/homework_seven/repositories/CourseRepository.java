package homework_seven.repositories;

import homework_seven.helper.DBHelper;
import homework_seven.models.Course;
import homework_seven.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CourseRepository implements CrudRepository<Course> {
    private Connection connection = DBHelper.getPostgresConnection();
    private static final String SELECT_BY_ID_FROM_COURSE = "SELECT * FROM course WHERE id = ";
    private static final String SELECT_BY_COURSE_ID_FROM_LESSON = "SELECT * FROM lesson WHERE course_id = ";
    private static final String SELECT_ALL_FROM_COURSE = "SELECT * FROM course";

    private RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        @Override
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(row.getInt("id"), row.getString("name"));
        }
    };

    private RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        @Override
        public Course mapRow(ResultSet row) throws SQLException {
            Integer id = row.getInt("id");
            Course course = new Course(id, row.getString("name"));
            Statement statement = connection.createStatement();
            ResultSet resultSetCourse = statement.executeQuery(SELECT_BY_COURSE_ID_FROM_LESSON + id);
            List<Lesson> lessons = new ArrayList<>();
            while (resultSetCourse.next()){
                Lesson lesson = lessonRowMapper.mapRow(resultSetCourse);
                lessons.add(lesson);
            }
            course.setLessons(lessons);
            return course;
        }
    };
    public Course find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSetCourse = statement.executeQuery(SELECT_BY_ID_FROM_COURSE + id);
            resultSetCourse.next();
            return courseRowMapper.mapRow(resultSetCourse);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Course> findAll() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSetCourse = statement.executeQuery(SELECT_ALL_FROM_COURSE);
            List<Course> courses = new ArrayList<>();
            while (resultSetCourse.next()){
                Course course = courseRowMapper.mapRow(resultSetCourse);
                courses.add(course);
            }
            return courses;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}