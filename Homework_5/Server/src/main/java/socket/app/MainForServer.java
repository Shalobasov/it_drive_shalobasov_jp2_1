package socket.app;

import com.beust.jcommander.JCommander;
import socket.argument.ArgumentServer;

public class MainForServer {
    public static void main(String[] args) {
        ArgumentServer argument = new ArgumentServer();
        JCommander.newBuilder().addObject(argument).build().parse(args);

        MultiServerSocket multiServerSocket = new MultiServerSocket();
        multiServerSocket.start(argument.getPort());
    }
}