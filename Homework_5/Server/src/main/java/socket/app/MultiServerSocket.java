package socket.app;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MultiServerSocket {
    private List<Socket> allClients = new ArrayList<>();

    public void start(int port) {
        try {
            final ServerSocket serverSocket = new ServerSocket(port);
            while (true) {
                final Socket currentClient = serverSocket.accept();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        connectToServer(currentClient);
                    }
                }).start();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void connectToServer(Socket currentClient) {
        try {
            allClients.add(currentClient);
            InputStream clientInput = currentClient.getInputStream();
            BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInput));
            String inputLine = clientReader.readLine();

            while (inputLine != null) {
                for (Socket client : allClients) {
                    PrintWriter writer = new PrintWriter(client.getOutputStream(), true);
                    writer.println(inputLine);
                }
                System.out.println(inputLine);
                inputLine = clientReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}