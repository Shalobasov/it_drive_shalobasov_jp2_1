package socket.argument;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class ArgumentServer {
    @Parameter(names = "--port")
    int port;

    public int getPort() {
        return port;
    }
}