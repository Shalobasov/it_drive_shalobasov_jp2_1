package socket.argument;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class ArgumentClient {
    @Parameter(names = "--serverPort")
    int serverPort;

    @Parameter(names = "--serverHost")
    String serverHost;

    public int getServerPort() {
        return serverPort;
    }

    public String getServerHost() {
        return serverHost;
    }
}