package socket.app;

import com.beust.jcommander.JCommander;
import socket.argument.ArgumentClient;

import java.util.Scanner;

public class MainForClient {
    public static void main(String[] args) {
        ArgumentClient argument = new ArgumentClient();
        JCommander.newBuilder().addObject(argument).build().parse(args);

        Scanner scanner = new Scanner(System.in);

        SocketClient socketClient = new SocketClient(argument.getServerHost(), argument.getServerPort());

        while (true) {
            String message = scanner.nextLine();
            socketClient.sendMessage(message);
        }
    }
}